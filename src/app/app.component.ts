import {NgsRevealModule} from 'ng-scrollreveal';
import {Component, TemplateRef} from '@angular/core';
import {trigger, state, style, transition, animate, keyframes} from '@angular/animations';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/modal-options.class';
import {LoginComponent} from './login/login.component';
import {FireserviceService} from "./fireservice.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [
    trigger('myAwesomeAnimation', []),
  ]
})
export class AppComponent {
  // title = 'app';
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService, public auth: FireserviceService) {
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  logout(){
    this.auth.logout();
    alert("You've just logged out");
  }
  
}
