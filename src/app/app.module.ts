import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgsRevealModule} from 'ng-scrollreveal';
import {AppComponent} from './app.component';
import {AlertModule, CarouselModule} from 'ngx-bootstrap';
import {ServicePageComponent} from './service-page/service-page.component';
import {PageAideComponent} from './page-aide/page-aide.component';
import {RouterModule} from '@angular/router';
import {PageGardeComponent} from './page-garde/page-garde.component';
import {PageAideDevoirComponent} from './page-aide-devoir/page-aide-devoir.component';
import {PageAideSeniorComponent} from './page-aide-senior/page-aide-senior.component';
import {PageJardinageComponent} from './page-jardinage/page-jardinage.component';
import {AboutComponent} from './about/about.component';
import {ModalModule} from 'ngx-bootstrap/modal';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {ResetpassComponent} from './resetpass/resetpass.component';
import {FireserviceService} from './fireservice.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {HttpModule} from '@angular/http';
import {FormBuilder, FormsModule} from "@angular/forms";
import { HistoryComponent } from './history/history.component';
import { EspaceComponent } from './espace/espace.component';
import { DatepickerModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ServicePageComponent,
    PageAideComponent,
    PageGardeComponent,
    PageAideDevoirComponent,
    PageAideSeniorComponent,
    PageJardinageComponent,
    AboutComponent,
    LoginComponent,
    SignupComponent,
    ResetpassComponent,
    HistoryComponent,
    EspaceComponent,
  ],
  imports: [
    FormsModule,
    HttpModule,
    BootstrapModalModule,
    BrowserModule,
    NgsRevealModule.forRoot(),
    BrowserAnimationsModule,
    CarouselModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    DatepickerModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'aide',
        component: PageAideComponent
      },
      {
        path: '',
        component: ServicePageComponent
      },
      {
        path: 'garde',
        component: PageGardeComponent
      },
      {
        path: 'devoir',
        component: PageAideDevoirComponent
      },
      {
        path: 'senior',
        component: PageAideSeniorComponent
      },
      {
        path: 'jardinage',
        component: PageJardinageComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'signup',
        component: SignupComponent
      },
      {
        path: 'resetpass',
        component: ResetpassComponent
      },
      {
        path: 'historique',
        component: HistoryComponent
      },
      {
        path: 'espace',
        component: EspaceComponent
      }
//       {
//       path: '',
//       redirectTo: '/',
//       pathMatch: 'full',
//       component:ServicePageComponent
// },
    ])
  ],
  providers: [FireserviceService, AngularFireAuth, FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule {
}
