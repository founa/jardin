import { Component, OnInit } from '@angular/core';
import {FireserviceService} from "../fireservice.service";
import { FirebaseObjectObservable, FirebaseListObservable } from "angularfire2/database";
@Component({
  selector: 'app-espace',
  templateUrl: './espace.component.html',
  styleUrls: ['./espace.component.sass']
})
export class EspaceComponent implements OnInit {
item: FirebaseObjectObservable<any>;
permission:boolean;
list: FirebaseListObservable<any[]>;
historique :FirebaseListObservable<any[]>;
commandes: FirebaseListObservable<any[]>;
datemin:Date;
datemax:Date;
 services = [
       {id: 1, name: "AIDE A DOMICILE"},
       {id: 2, name: "AIDES AUX DEVOIRS"},
       {id: 3, name: "AIDES AUX SENIORS"},
       {id: 4, name: "GARDE D'ENFATNS"},
       {id: 5, name: "JARDINAGE/BRICOLAGE"},
     ];
    selectedService = null;
selectednumber = 0;
     Types = [
       {id: 1, name: "ponctuelle"},
       {id: 2, name: "annuelle"},
     ];
    selectedType = null;

     Mode = [
       {id: 1, name: "CESU bancaire"},
       {id: 2, name: "à l'agence"},
     ];
    selectedMode = null;
 constructor(public auth: FireserviceService) {
         this.historique = this.auth.AfDB.list('/utilisateurs/'+this.auth.getUid()+'/historique');
   this.list = auth.AfDB.list('/utilisateurs/');
console.log('/utilisateurs/'+auth.getUid());
   this.item = auth.AfDB.object('/utilisateurs/'+auth.getUid(), { preserveSnapshot: true });
   this.item.subscribe(snapshot => {
 if(snapshot.child('type').val() == "client"){
    this.permission = false;
 } 
 else{
   this.permission = true;
  this.commandes = auth.AfDB.list('/commandes');

 }

});


  }
  ngOnInit() {
  }
  commander(){
    if(this.selectedMode!=null && this.selectedService !=null && this.selectedType !=null ){
      if(this.selectedType.id ==2){
        if(this.datemin == null || this.datemax == null){
          return;
        }
      }
      else{
        if(this.datemin == null ){
          return;
        }
      }
      console.log("can command");
      let obj = {'selectedMode':this.selectedMode.name,'selectedService':this.selectedService.name,'selectedType':this.selectedType.name};
      if(this.selectedService.id==4){
        obj['nombre'] = this.selectednumber;
      }
      if(this.selectedType.id == 2){
           obj['datemin'] = this.datemin;
            obj['datemax'] = this.datemax;


      }
      else{
                   obj['datemin'] = this.datemin;

      }
      let aRef = this.historique.push(obj).key;
      console.log(aRef);
      obj['id']=aRef;
      obj['verified']=false;
      this.auth.AfDB.object('/utilisateurs/'+this.auth.getUid()+'/historique/'+aRef).update(obj);
obj['userId']=this.auth.getUid();
          this.auth.AfDB.object('/commandes/'+aRef+"/").update(obj);
          this.selectedService = null;
          this.selectedMode=null;
          this.selectedType=null;
          this.selectednumber=null;
          this.datemin = null;
          this.datemax = null;

    }
  }
approve(id,userid){
  console.log(id);
  this.auth.AfDB.object('/utilisateurs/'+userid+'/historique/'+id+'/verified/').set(true);
  this.auth.AfDB.object('/commandes/'+id+'/verified/').set(true);
}
}
