import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

@Injectable()
export class FireserviceService {
  users: FirebaseListObservable<any[]>
  item: FirebaseObjectObservable<any>;

  constructor(public afa: AngularFireAuth, public router: Router,
              public AfDB: AngularFireDatabase) {
  }


  signinUser(email: string, password: string) {
    this.afa.auth.signInWithEmailAndPassword(email, password)
      .then(
        response => {
          if (this.afa.auth.currentUser.emailVerified) {
            this.router.navigate(['/']);
            this.afa.auth.currentUser.getToken()
              .then(
                (token: string) => {
                  localStorage.setItem('token', token);
                  localStorage.setItem('uid', this.afa.auth.currentUser.uid);
                  localStorage.setItem('email', this.afa.auth.currentUser.email);
                }
              );
          } else { 
            this.logout();
             this.router.navigate(['/']);
            alert('Please verify your email');
          }
        }
      )
      .catch(
        error => console.log(error)
      );
  }

  registerUser(email, password, forme) {
    this.afa.auth.createUserWithEmailAndPassword(email, password)
      .then(
        (success) => {
          this.users = this.AfDB.list('/utilisateurs');
          forme.uid = this.afa.auth.currentUser.uid;
         
          this.AfDB.object('utilisateurs/'+forme.uid+"/").set(forme);
          let user: any = this.afa.auth.currentUser;
          user.sendEmailVerification().then(
            (success) => {
               this.router.navigate(['/']);
              this.logout();
              alert('please verify your email');
            }
          ).catch(
            (err) => {
              console.log('Error:' + err);
              alert('Error:' + err);
            }
          );
        })
      .catch(
        error => alert(error.message)
      );
  }

  logout() {
    this.afa.auth.signOut();
    localStorage.clear();
  }

  getEmail() {
    return localStorage.getItem('email');
  }

  getToken() {
    return localStorage.getItem('token')
  }
  getUid() {
    return localStorage.getItem('uid')
  }

  isAuthenticated() {
    return localStorage.getItem('token') != null;
  }

  get currentUser(): string {
    return this.afa.auth.currentUser ? this.afa.auth.currentUser.uid : null;
  }


  resetPassword(emailAddress: string) {
    return Observable.create(observer => {
      this.afa.auth.sendPasswordResetEmail(emailAddress).then((success) => {
        // console.log('email sent', success);
        observer.next(success);
      }, (error) => {
        // console.log('error sending email',error);
        observer.error(error);
      });
    });
  }

}
