import { Component, OnInit } from '@angular/core';
import {FireserviceService} from '../fireservice.service';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.sass']
})
export class HistoryComponent implements OnInit {
  users: FirebaseListObservable<any[]>;
  history;

  constructor(public auth: FireserviceService, public AfDatabase: AngularFireDatabase) {
      if (auth.isAuthenticated()) {
        this.users = AfDatabase.list('/utilisateurs');
        this.users.forEach(users => {
          for (let user of users) {
            if (auth.getUid() === user.uid) {
              this.history = JSON.parse(user.historique);
              console.log(this.history);
              return;
            }
          }
        });
      }

  }

  ngOnInit() {
  }

}
