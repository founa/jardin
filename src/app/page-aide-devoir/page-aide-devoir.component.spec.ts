import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAideDevoirComponent } from './page-aide-devoir.component';

describe('PageAideDevoirComponent', () => {
  let component: PageAideDevoirComponent;
  let fixture: ComponentFixture<PageAideDevoirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAideDevoirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAideDevoirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
