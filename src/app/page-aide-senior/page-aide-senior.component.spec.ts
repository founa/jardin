import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAideSeniorComponent } from './page-aide-senior.component';

describe('PageAideSeniorComponent', () => {
  let component: PageAideSeniorComponent;
  let fixture: ComponentFixture<PageAideSeniorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAideSeniorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAideSeniorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
