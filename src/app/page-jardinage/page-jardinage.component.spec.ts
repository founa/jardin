import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageJardinageComponent } from './page-jardinage.component';

describe('PageJardinageComponent', () => {
  let component: PageJardinageComponent;
  let fixture: ComponentFixture<PageJardinageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageJardinageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageJardinageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
