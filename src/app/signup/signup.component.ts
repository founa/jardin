import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {FireserviceService} from '../fireservice.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  constructor(public auth: FireserviceService) { }

  ngOnInit() {
  }

  onSignup(form: NgForm) {
    if (form.value.email != form.value.conf_email) {
      alert("Email fields doesn't match");
      return;
    }
    if (form.value.password != form.value.conf_password) {
      alert("Passwords fields doesn't match");
      return;
    }
    if (!Number(form.value.portable) || !Number(form.value.fix)) {
      alert("Please enter a valid phone number");
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    const forme = {
      email: form.value.email,
      addresse_postale: form.value.addpost,
      tel_portable: form.value.portable,
      tel_fix: form.value.fix,
      nom: form.value.nom,
      prenom: form.value.prenom,
      historique : "[]",
      type: 'client'
    };
    this.auth.registerUser(email, password, forme);
  }

}
